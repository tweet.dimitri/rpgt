"""
Main file
"""


def main() -> None:
    """
    Main function
    """
    print("Hello world!")


if __name__ == "__main__":
    main()
