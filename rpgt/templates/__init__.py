"""
Templates module.
"""

import rpgt.templates.code
import rpgt.templates.code.pytorch
import rpgt.templates.code.sklearn
import rpgt.templates.code.stablebaselines
import rpgt.templates.configs
import rpgt.templates.docker
import rpgt.templates.dvc
import rpgt.templates.gitignore
import rpgt.templates.licenses
import rpgt.templates.tests
import rpgt.templates.tests.unit
import rpgt.templates.version_control.issue_templates
import rpgt.templates.version_control.merge_request_templates
