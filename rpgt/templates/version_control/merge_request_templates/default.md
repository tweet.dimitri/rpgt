<!-- Change #i to the corresponding issue e.g. #1 -->
Closes #i

## Description

<!-- What feature does this MR add/What bug does this MR fix? -->

## Checks
- [ ] The program works and compiles.
<!-- Copy the definition of done from your issue here -->
- [ ] The added functionality is tested with unit tests with at least 90% of branch coverage.
- [ ] The added functionality is documented with JSDoc or javadoc.
